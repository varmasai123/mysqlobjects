/**************************************************************************
          Copyright 2019 AYN, Inc.  All Rights Reserved
***************************************************************************
View Name:   
---------------------------------------------------------------------------
 Source History
 --------------
MOD#      Date      Who   	 Description
---------------------------------------------------------------------------
001	  	 07/30/2019           Update Contact Company

***************************************************************************/
DELIMITER $$

DROP PROCEDURE IF  EXISTS upd_contact_company $$


CREATE PROCEDURE upd_contact_company
(

   /* parameters */

     OUT return_val   INTEGER   
	,p_contact_company_key bigint  
	,p_email_addr varchar(40)  
	,p_first_name varchar(20)  
	,p_Last_name varchar(20)   
	,p_business_name varchar(135)  
    ,p_phone_nbr varchar(25) 
	,p_mobile_phone_nbr varchar(25) 
	,p_fax_nbr varchar(25) 
	,p_addr1 varchar(40) 
	,p_addr2 varchar(40) 
	,p_addr3 varchar(40) 
	,p_city varchar(20) 
	,p_state_name varchar(5) 
	,p_country varchar(15) 
	,p_zip varchar(12) 
	,p_country_code varchar(5) 
	,p_std_bill_rate numeric(28,9)
	,p_base_currecny varchar(5) 
	,p_contact_date datetime 
	,p_job_auto_number varchar(1) 
	,p_contact_status varchar(1) 
	,p_Doc_type varchar(5) 

)

BEGIN

             /* Local Variables */


    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    
BEGIN
GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
 @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
SET @full_error = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @text);
SELECT @full_error;
END;
	  
       
   /********** INITIALIZING LOCAL VARIABLES **********/

    
    
       IF EXISTS(SELECT 1 FROM contact_company  
          WHERE contact_company_key  = p_contact_company_key ) THEN
	   BEGIN
 
       /* Update contact_company */

       UPDATE contact_company
       SET   email_addr           = p_email_addr
	        ,first_name           = p_first_name  
	        ,Last_name            = p_Last_name  
	        ,business_name        = p_business_name
            ,phone_nbr            = p_phone_nbr
	        ,mobile_phone_nbr     = p_mobile_phone_nbr
	        ,fax_nbr              = p_fax_nbr
	        ,addr1                = p_addr1
	        ,addr2                = p_addr2
	        ,addr3                = p_addr3
	       ,city                  = p_city
	       ,state_name            = p_state_name
	       ,country               = p_country
	       ,zip                   = p_zip
	       ,country_code          = p_country_code
	       ,std_bill_rate         = p_std_bill_rate
	       ,base_currecny         = p_base_currecny
	       ,contact_date          = p_contact_date
	       ,job_auto_number       = p_job_auto_number
	       ,contact_status        = p_contact_status 
	       ,Doc_type              = p_Doc_type

       WHERE contact_company_key  = p_contact_company_key;
       END;
       ELSE BEGIN
             if ( p_email_addr  is not null     and
                  p_first_name is not null   and
                  p_Last_name is not null   and
                  p_business_name is not null   and
                  p_phone_nbr     is not null   and
                  p_addr1         is not null   and
                  p_city          is not null   and
                  p_state_name    is not  null   and
                  p_country       is not null   and
                  p_zip           is not null   and
                  p_base_currecny is not null   
                ) then

                /* Insert into contact_company */
              INSERT INTO contact_company(  contact_company_key
                                           ,email_addr           
	                                       ,first_name            
	                                       ,Last_name            
	                                       ,business_name        
                                           ,phone_nbr           
	                                       ,mobile_phone_nbr     
	                                       ,fax_nbr              
	                                       ,addr1                
	                                       ,addr2                
	                                       ,addr3               
	                                       ,city                 
	                                      ,state_name            
	                                      ,country               
	                                      ,zip                   
	                                      ,country_code          
	                                      ,std_bill_rate         
	                                      ,base_currecny        
	                                      ,contact_date         
	                                     ,job_auto_number      
	                                     ,contact_status         
	                                     ,Doc_type             
            )
            VALUES(
                p_contact_company_key
               ,p_email_addr
               ,p_first_name 
               ,p_Last_name  
               ,p_business_name
               ,p_phone_nbr
               ,p_mobile_phone_nbr
               ,p_fax_nbr
               ,p_addr1
               ,p_addr2
               ,p_addr3
               ,p_city
               ,p_state_name
               ,p_country
               ,p_zip
               ,p_country_code
               ,p_std_bill_rate
               ,p_base_currecny
               ,p_contact_date
               ,p_job_auto_number
               ,p_contact_status
               ,p_Doc_type
            );
         END if;
	 END;
     END if;
     select * from contact_company;
END $$  /* end of procedure */
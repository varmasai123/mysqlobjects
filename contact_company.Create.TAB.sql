DROP TABLE  IF EXISTS syn.contact_company;

CREATE TABLE IF NOT EXISTS syn.contact_company
	(
	contact_company_key bigint  auto_increment  NOT NULL,
	email_addr varchar(40) NOT NULL,
	first_name varchar(20)  NOT NULL,
	Last_name varchar(20)  NOT NULL,
	business_name varchar(135) NOT NULL,
    phone_nbr varchar(25) NULL,
	mobile_phone_nbr varchar(25) NULL,
	fax_nbr varchar(25) NULL,
	addr1 varchar(40) NULL,
	addr2 varchar(40) NULL,
	addr3 varchar(40) NULL,
	city varchar(20) NULL,
	state_name varchar(5) NULL,
	country varchar(15) NULL,
	zip varchar(12) NULL,
	country_code varchar(5) NULL,
	std_bill_rate numeric(28,9),
	base_currecny varchar(5) NULL,
	contact_date datetime NULL,
	job_auto_number varchar(1) NULL,
	contact_status varchar(1) NULL,
	Doc_type varchar(5) NULL,
	PRIMARY KEY (contact_company_key)
	);
